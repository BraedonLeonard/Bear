(* Defines the structure of the AST *)

open Ast.Ast_types

type expr =
    | Integer of loc * int
    | Array of loc * expr list
    | Func of loc * parameter list * expr list
    | Operator of loc * operator
    | Application of loc * expr * expr option list
    | FieldAccess of loc * expr * string
    | Var of loc * type_expr option * string * expr
    | TypeConstructor of loc * string
    | Identifier of loc * string
    | If of loc * expr * expr * expr

type top_level_expr =
    | ProductType of loc * string * product_type_field list
    | GeneralExpr of expr

type program = Program of top_level_expr list
