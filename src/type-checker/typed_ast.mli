open Ast.Ast_types

(*
Encodes the type_expr, plus optional extra data
e.g. Encode TEFunction type, and list of all functions which could be evaluated when evaluating
     the expression
*)
type type_info =
  | TIInt
  | TIArray of type_info
  (* Possible functions, parameter types, return type *)
  | TIFunction of applicative list * type_info list * type_info
  | TIProduct of string (* string is the type_id *)
  | TIVariable of string (* string is the name of the type variable *)
and applicative =
  (* (operator, applied args) *)
  | AppOperator of operator * type_info option list
  (* (function params, function body, applied args) *)
  | AppFunction of parameter list * Parsed_ast.expr list * type_info option list
  (* (type id, applied args) *)
  | AppTypeConstructor of string * type_info option list
  (* (operator which created the derived function (e.g. OpEach), type info of the function being derived from, applied args) *)
  | AppDerivedFunction of operator * type_info * type_info option list

type expr =
  | Integer of loc * int
  | Array of loc * type_info * expr list
  | Func of loc * type_info * parameter list * Parsed_ast.expr list
  | Operator of loc * type_info * operator
  | Application of loc * type_info * expr * expr option list
  | FieldAccess of loc * type_info * expr * string
  | Var of loc * type_info * string * expr
  | TypeConstructor of loc * type_info * string
  | Identifier of loc * type_info * string
  | If of loc * type_info * expr * expr * expr

type top_level_expr =
  | ProductType of loc * string * product_type_field list
  | GeneralExpr of expr

type program = Program of top_level_expr list

val type_info_to_string : type_info -> string

val upcast_type_expr_to_type_info : type_expr -> type_info