open Core

let res =
  let open Result in
  Parse.parse (Lexing.from_channel (In_channel.create Sys.argv.(1))) >>= fun ast ->
  Printf.printf "Parsed AST:\n";
  Ok(Parse.print_parse_tree ast)

let _ =
  match res with
  | Error e -> Printf.printf "%s\n" (Error.to_string_hum e)
  | Ok _ -> Printf.printf "Done!\n"
