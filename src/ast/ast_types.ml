open Core
(* Defines the types used in the AST *)

(* Stores the line and position of the token *)
type loc = Lexing.position

(* built-in operators *)
type operator =
    | OpPlus
    | OpMinus
    | OpMult
    | OpDiv
    | OpLeq
    | OpGeq
    | OpNeq
    | OpLess
    | OpGreater
    | OpMod
    | OpEqual
    | OpEach
    | OpScan
    | OpOver
    | OpJoin
    | OpTil

(* type expressions *)
type type_expr =
    | TEInt
    | TEArray of type_expr (* Array of type `type_expr` *)
    | TEFunction of type_expr list * type_expr (* Parameter types, return type *)
    | TEProduct of string (* string is the type id *)
    | TEVariable of string (* string is the name of the type variable *)

type parameter = { id : string; te_maybe : type_expr option }

type product_type_field = { name : string; te : type_expr }

(* to_string functions *)

let loc_to_string loc =
    Fmt.str "Line:%d Position:%d" loc.Lexing.pos_lnum
        (loc.Lexing.pos_cnum - loc.Lexing.pos_bol + 1)

(* to_string function for each ast type *)
let operator_to_string op =
    match op with
    | OpPlus -> "+"
    | OpMinus -> "-"
    | OpMult -> "*"
    | OpDiv -> "%"
    | OpLeq -> "<="
    | OpGeq -> ">="
    | OpNeq -> "<>"
    | OpLess -> "<"
    | OpGreater -> ">"
    | OpMod -> "mod"
    | OpEqual -> "="
    | OpEach -> "~"
    | OpScan -> "\\"
    | OpOver -> "/"
    | OpJoin -> ","
    | OpTil -> "til"

let rec type_expr_to_string te =
    match te with
    | TEInt -> "Int" (* TODO: Link this with what the tokenizer actually looks for*)
    | TEArray type_expr -> Fmt.str "Array (%s)" (type_expr_to_string type_expr)
    | TEFunction(arg_tes, return_te) -> Fmt.str "(%s) -> %s"
        (String.concat ~sep: "; " (List.map ~f: type_expr_to_string arg_tes))
        (type_expr_to_string return_te)
    | TEProduct type_id -> type_id
    | TEVariable id -> Fmt.str "%s'" id

let parameter_to_string {id=id; te_maybe=te_maybe} =
    match te_maybe with
    | Some te -> id ^ (Fmt.str " : %s" (type_expr_to_string te))
    | None -> id