open Core

let res =
  let open Result in
  (* match Parse.parse (Lexing.from_channel (In_channel.create Sys.argv.(1))) with
    | Ok pt -> Parse.print_parse_tree pt
    | Error e -> Printf.printf "%s" (Error.to_string_hum e);; *)
  Parse.parse (Lexing.from_channel (In_channel.create Sys.argv.(1))) >>= fun ast ->
  Printf.printf "Parsed AST:\n";
  Parse.print_parse_tree ast;
  Type_check.type_program ast >>= fun typed_ast ->
  Printf.printf "\nTyped AST:\n";
  Type_check.print_typed_ast typed_ast;
  Interpret.interpret_program typed_ast >>= fun result -> Ok(result)

let _ =
  match res with
  | Error e -> Printf.printf "%s\n" (Error.to_string_hum e)
  | Ok value -> Printf.printf "\nResult: %s\n" (Interpret.interpreter_result_to_string value)
